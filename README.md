# PJS.Bootstrap #

Orchard CMS Theme: I have taken a different approach with the latest version of this theme, overriding as few Orchard shapes as possible and injecting as many changes as possible via LESS. In doing so, the structure of your website can be easily modified using this theme. The Blog shapes have been purposely kept very generic so that the user of the theme can easily change the blog layout to suit their needs.

### Compatibility ###

* Requires Orchard 1.8 or higher
* Built with LESS

### Installation ###

* Package available for download in the [Orchard Gallery](http://gallery.orchardproject.net/List/Themes/Orchard.Theme.PJS.Bootstrap)
* Demo available [here](http://philipsenechal.com/bootstrap)

### Usage ###

Includes Admin panel options for:

* Boxed or Fluid Layout
* Fixed Top Navigation or Floating Navigation
* Primary or Inverse Navigation Bar Color
* Navigation Bar Search Field
* Sticky or Normal Footer
* Select a [Bootswatch Theme](http://bootswatch.com) or Default Bootstrap Style

### Included Dependencies ###

* Bootstrap 3.2.0
* Font Awesome 4.2.0
* Isotope 1.5.25
* Validation Engine 2.6.2
* prettyPhoto 3.1.5

### Custom Module Shapes ###

* BlogPost (add a MediaLibraryPickerField named BlogPostImage to the BlogPost Part to use the image feature)
* Gravatar (Comments)
* LatestTwitter
* Zen Gallery (add a comma separated list of Categories in the Alternate Text field of the images via the Orchard Media Library to enable filtering on the Gallery page)